#include <stdio.h>

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)

void CheckError(cl_int p_Error, const char* p_Msg)
{
    if (p_Error != CL_SUCCESS)
    {
        fprintf(stderr, "%s [%d]\n", p_Msg, p_Error);
    }
}

void RunOpenCLKernel(void* p_CmdQ, int p_Width, int p_Height, float* p_transform,
	float p_lum, float p_alpha, const float* p_Input, float* p_Output)
{
    cl_int error;

    cl_command_queue cmdQ = static_cast<cl_command_queue>(p_CmdQ);

    static cl_context clContext = NULL;
    if (clContext == NULL)
    {
        error = clGetCommandQueueInfo(cmdQ, CL_QUEUE_CONTEXT, sizeof(cl_context), &clContext, NULL);
        CheckError(error, "Unable to get the context");
    }

    static cl_device_id deviceId = NULL;
    if (deviceId == NULL)
    {
        error = clGetCommandQueueInfo(cmdQ, CL_QUEUE_DEVICE, sizeof(cl_device_id), &deviceId, NULL);
        CheckError(error, "Unable to get the device");
    }

    FILE *fp;
    char fileName[] = "/Users/pieter/Dev/HDRShader/OpenFX/HDRPlugin/transform.cl";
    char *source_str;
    size_t source_size;

    /* Load the source code containing the kernel*/
    fp = fopen(fileName, "r");
    if (!fp) {
      fprintf(stderr, "Failed to load kernel.\n");
      exit(1);
    }

    source_str = (char*)malloc(MAX_SOURCE_SIZE);
    source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
    fclose(fp);

    static cl_kernel kernel = NULL;
    if (kernel == NULL)
    {
        cl_program program = clCreateProgramWithSource(clContext, 1, (const char **)&source_str, (const size_t *)&source_size, &error);
        CheckError(error, "Unable to create program");

        error = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
        CheckError(error, "Unable to build program");

        kernel = clCreateKernel(program, "HdrAdjustKernel", &error);
        CheckError(error, "Unable to create kernel");
    }

    int count = 0;
    error  = clSetKernelArg(kernel, count++, sizeof(int), &p_Width);
    error |= clSetKernelArg(kernel, count++, sizeof(int), &p_Height);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[0]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[1]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[2]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[3]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[4]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[5]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[6]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[7]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_transform[8]);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_lum);
    error |= clSetKernelArg(kernel, count++, sizeof(float), &p_alpha);
    error |= clSetKernelArg(kernel, count++, sizeof(cl_mem), &p_Input);
    error |= clSetKernelArg(kernel, count++, sizeof(cl_mem), &p_Output);
    CheckError(error, "Unable to set kernel arguments");

    size_t localWorkSize[2], globalWorkSize[2];
    clGetKernelWorkGroupInfo(kernel, deviceId, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), localWorkSize, NULL);
    localWorkSize[1] = 1;
    globalWorkSize[0] = ((p_Width + localWorkSize[0] - 1) / localWorkSize[0]) * localWorkSize[0];
    globalWorkSize[1] = p_Height;

    clEnqueueNDRangeKernel(cmdQ, kernel, 2, NULL, globalWorkSize, localWorkSize, 0, NULL, NULL);
}
