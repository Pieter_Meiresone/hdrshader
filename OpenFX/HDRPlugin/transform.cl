float4 RGBtoXYZ( float4 RGBA, float RGB2XYZ_XR, float RGB2XYZ_XG, float RGB2XYZ_XB,
 			float RGB2XYZ_YR, float RGB2XYZ_YG, float RGB2XYZ_YB,
			float RGB2XYZ_ZR, float RGB2XYZ_ZG, float RGB2XYZ_ZB)
{
	float X = RGB2XYZ_XR * RGBA.x + RGB2XYZ_XG * RGBA.y + RGB2XYZ_XB * RGBA.z;
	float Y = RGB2XYZ_YR * RGBA.x + RGB2XYZ_YG * RGBA.y + RGB2XYZ_YB * RGBA.z;
	float Z = RGB2XYZ_ZR * RGBA.x + RGB2XYZ_ZG * RGBA.y + RGB2XYZ_ZB * RGBA.z;
	return (float4)(X, Y, Z, RGBA.w);
}

float4 XYZtoYUV( float4 XYZA )
{
    float Y = XYZA.y;
    float U = (((1626.6875 * XYZA.x) / (XYZA.x + 15.0 * XYZA.y + 3.0 * XYZA.z)) + 0.546875) * 4.0;
    float V = (((3660.046875 * XYZA.y) / (XYZA.x + 15.0 * XYZA.y + 3.0 * XYZA.z)) + 0.546875) * 4.0;
    return (float4)(Y, U, V, XYZA.w);
}

__kernel void HdrAdjustKernel(
   int p_Width,
   int p_Height,
   float p_xr,
   float p_xg,
   float p_xb,
   float p_yr,
   float p_yg,
   float p_yb,
   float p_zr,
   float p_zg,
   float p_zb,
   float p_lum,
   float p_alpha,
   __global const float* p_Input,
   __global float* p_Output)
{
   const int x = get_global_id(0);
   const int y = get_global_id(1);

   if ((x < p_Width) && (y < p_Height))
   {
       const int index = ((y * p_Width) + x) * 4;

       if(y % 20 == 0) {
           // Test section to see if the code is working
           p_Output[index + 0] = 1;
           p_Output[index + 1] = 0;
           p_Output[index + 2] = 0;
           p_Output[index + 3] = 0;
       } else {
           float sc = 1.f * p_lum;
           float4 input = (float4)(p_Input[index + 0],
                                   p_Input[index + 1],
                                   p_Input[index + 2],
                                   p_Input[index + 3]);
           input *= sc;

           float4 yuva = XYZtoYUV(RGBtoXYZ(input, p_xr, p_xg, p_xb, p_yr, p_yg, p_yb, p_zr, p_zg, p_zb));

           // Deinterlacing
           if(x == 0) {
               const int rindex = ((y * p_Width) + x + 1) * 4;
               float4 right = (float4)(p_Input[rindex + 0],
                                       p_Input[rindex + 1],
                                       p_Input[rindex + 2],
                                       p_Input[rindex + 3]);
               right *= sc;
               float4 yuvaRight = XYZtoYUV(RGBtoXYZ(right, p_xr, p_xg, p_xb, p_yr, p_yg, p_yb, p_zr, p_zg, p_zb));

               yuva.s1 += yuvaRight.s1;
               yuva.s2 += yuvaRight.s2;

               yuva.s1 /= 2;
               yuva.s2 /= 2;

           } else if(x == 1919) {
               const int lindex = ((y * p_Width) + x - 1) * 4;
               float4 left = (float4)(p_Input[lindex + 0],
                                      p_Input[lindex + 1],
                                      p_Input[lindex + 2],
                                      p_Input[lindex + 3]);
               left *= sc;
               float4 yuvaLeft = XYZtoYUV(RGBtoXYZ(left, p_xr, p_xg, p_xb, p_yr, p_yg, p_yb, p_zr, p_zg, p_zb));

               yuva.s1 += yuvaLeft.s1;
               yuva.s2 += yuvaLeft.s2;

               yuva.s1 /= 2;
               yuva.s2 /= 2;
           } else {
               const int lindex = ((y * p_Width) + x - 1) * 4;
               float4 left = (float4)(p_Input[lindex + 0],
                                      p_Input[lindex + 1],
                                      p_Input[lindex + 2],
                                      p_Input[lindex + 3]);
               left *= sc;
               float4 yuvaLeft = XYZtoYUV(RGBtoXYZ(left, p_xr, p_xg, p_xb, p_yr, p_yg, p_yb, p_zr, p_zg, p_zb));

               yuva.s1 += yuvaLeft.s1;
               yuva.s2 += yuvaLeft.s2;

               const int rindex = ((y * p_Width) + x + 1) * 4;
               float4 right = (float4)(p_Input[rindex + 0],
                                      p_Input[rindex + 1],
                                      p_Input[rindex + 2],
                                      p_Input[rindex + 3]);
               right *= sc;
               float4 yuvaRight = XYZtoYUV(RGBtoXYZ(right, p_xr, p_xg, p_xb, p_yr, p_yg, p_yb, p_zr, p_zg, p_zb));

               yuva.s1 += yuvaRight.s1;
               yuva.s2 += yuvaRight.s2;

               yuva.s1 /= 3;
               yuva.s2 /= 3;
           }

           // Conversion to SIM 2 specific format
           float Lh, Ll, Ch, Cl;
           float L = (yuva.s0 < 0.00001) ? 0 : ((p_alpha * log2(yuva.s0)) + 0.5);
           L = floor((253 * L + 1) * 32 + 0.5);
           L = clamp(L, 32.0f, 8159.0f);
           Lh = floor(L / 32.0);
           Ll = L - (Lh * 32.0);

           float3 out = (float3)(0.0f);

           if(x % 2 == 0) { // Pari
               float V = floor(yuva.s2 + 0.5);
               V = clamp(V, 4.0f, 1019.0f);
               Ch = floor(V / 4.0);
               Cl = V - (Ch * 4.0);

               out.s0 = clamp((Ll * 8.0f) + (Cl * 2.0f), 1.0f, 254.0f);
               out.s1 = Lh;
               out.s2 = Ch;
           } else { // Dispari
               float U = floor(yuva.s1 + 0.5);
               U = clamp(U, 4.0f, 1019.0f);
               Ch = floor(U / 4.0);
               Cl = U - (Ch * 4.0);

               out.s0 = Ch;
               out.s1 = Lh;
               out.s2 = clamp((Ll * 8.0f) + (Cl * 2.0f), 1.0f, 254.0f);
           }

           out /= 255.0;

           p_Output[index + 0] = out.s0;
           p_Output[index + 1] = out.s1;
           p_Output[index + 2] = out.s2;
           p_Output[index + 3] = 1.0;
       }
   }
}
