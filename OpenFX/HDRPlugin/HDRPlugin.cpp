#include "HDRPlugin.h"

#include <stdio.h>

#include "ofxsImageEffect.h"
#include "ofxsMultiThread.h"
#include "ofxsProcessing.h"
#include "ofxsLog.h"

#define kPluginName "HDR Transform"
#define kPluginGrouping "ETRO VUB"
#define kPluginDescription "Color space transform suited for a SIM 2 display"
#define kPluginIdentifier "etro.vub.ac.be.HDR"
#define kPluginVersionMajor 1
#define kPluginVersionMinor 0

#define kSupportsTiles false
#define kSupportsMultiResolution false
#define kSupportsMultipleClipPARs false

////////////////////////////////////////////////////////////////////////////////

class ImageScaler : public OFX::ImageProcessor
{
public:
    explicit ImageScaler(OFX::ImageEffect& p_Instance);

    virtual void processImagesCUDA();
    virtual void processImagesOpenCL();
    virtual void multiThreadProcessImages(OfxRectI p_ProcWindow);

    void setSrcImg(OFX::Image* p_SrcImg);
    void setParams(float p_xr, float p_xg, float p_xb,
        float p_yr, float p_yg, float p_yb,
        float p_zr, float p_zg, float p_zb,
        float p_lum, float p_alpha);

private:
    OFX::Image* _srcImg;
    float _transform[9];
    float _lum_boost;
    float _alpha;
};

ImageScaler::ImageScaler(OFX::ImageEffect& p_Instance)
    : OFX::ImageProcessor(p_Instance)
{
}

//extern void RunCudaKernel(int p_Width, int p_Height, float* p_Gain, const float* p_Input, float* p_Output);

void ImageScaler::processImagesCUDA()
{
    // const OfxRectI& bounds = _srcImg->getBounds();
    // const int width = bounds.x2 - bounds.x1;
    // const int height = bounds.y2 - bounds.y1;
    //
    // float* input = static_cast<float*>(_srcImg->getPixelData());
    // float* output = static_cast<float*>(_dstImg->getPixelData());

    // RunCudaKernel(width, height, _scales, input, output);
}

extern void RunOpenCLKernel(void* p_CmdQ, int p_Width, int p_Height,
	float* p_transform, float p_lum, float p_alpha, const float* p_Input, float* p_Output);

void ImageScaler::processImagesOpenCL()
{
    const OfxRectI& bounds = _srcImg->getBounds();
    const int width = bounds.x2 - bounds.x1;
    const int height = bounds.y2 - bounds.y1;

    float* input = static_cast<float*>(_srcImg->getPixelData());
    float* output = static_cast<float*>(_dstImg->getPixelData());

	RunOpenCLKernel(_pOpenCLCmdQ, width, height, _transform, _lum_boost, _alpha, input, output);
}

void ImageScaler::multiThreadProcessImages(OfxRectI p_ProcWindow)
{
    // for (int y = p_ProcWindow.y1; y < p_ProcWindow.y2; ++y)
    // {
    //     if (_effect.abort()) break;
    //
    //     float* dstPix = static_cast<float*>(_dstImg->getPixelAddress(p_ProcWindow.x1, y));
    //
    //     for (int x = p_ProcWindow.x1; x < p_ProcWindow.x2; ++x)
    //     {
    //         float* srcPix = static_cast<float*>(_srcImg ? _srcImg->getPixelAddress(x, y) : 0);
    //
    //         // do we have a source image to scale up
    //         if (srcPix)
    //         {
    //             for(int c = 0; c < 4; ++c)
    //             {
    //                 dstPix[c] = srcPix[c] * _scales[c];
    //             }
    //         }
    //         else
    //         {
    //             // no src pixel here, be black and transparent
    //             for (int c = 0; c < 4; ++c)
    //             {
    //                 dstPix[c] = 0;
    //             }
    //         }
    //
    //         // increment the dst pixel
    //         dstPix += 4;
    //     }
    // }
}

void ImageScaler::setSrcImg(OFX::Image* p_SrcImg)
{
    _srcImg = p_SrcImg;
}

void ImageScaler::setParams(float p_xr, float p_xg, float p_xb,
    float p_yr, float p_yg, float p_yb,
    float p_zr, float p_zg, float p_zb,
    float p_lum, float p_alpha)
{
    _transform[0] = p_xr;
    _transform[1] = p_xg;
    _transform[2] = p_xb;
    _transform[3] = p_yr;
    _transform[4] = p_yg;
    _transform[5] = p_yb;
    _transform[6] = p_zr;
    _transform[7] = p_zg;
    _transform[8] = p_zb;

    _lum_boost = p_lum;
    _alpha = p_alpha;
}

////////////////////////////////////////////////////////////////////////////////
/** @brief The plugin that does our work */
class HDRPlugin : public OFX::ImageEffect
{
public:
    explicit HDRPlugin(OfxImageEffectHandle p_Handle);

    /* Override the render */
    virtual void render(const OFX::RenderArguments& p_Args);

    /* Override is identity */
    virtual bool isIdentity(const OFX::IsIdentityArguments& p_Args, OFX::Clip*& p_IdentityClip, double& p_IdentityTime);

    /* Override changedParam */
    virtual void changedParam(const OFX::InstanceChangedArgs& p_Args, const std::string& p_ParamName);

    /* Override changed clip */
    virtual void changedClip(const OFX::InstanceChangedArgs& p_Args, const std::string& p_ClipName);

    /* Set the enabledness of the scale params depending on the type of input image */
    void setEnabledness();

    /* Set up and run a processor */
    void setupAndProcess(ImageScaler &p_ImageScaler, const OFX::RenderArguments& p_Args);

private:
    // Does not own the following pointers
    OFX::Clip* m_DstClip;
    OFX::Clip* m_SrcClip;

    OFX::DoubleParam* m_RGB2XYZ_XR;
    OFX::DoubleParam* m_RGB2XYZ_XG;
    OFX::DoubleParam* m_RGB2XYZ_XB;
    OFX::DoubleParam* m_RGB2XYZ_YR;
    OFX::DoubleParam* m_RGB2XYZ_YG;
    OFX::DoubleParam* m_RGB2XYZ_YB;
    OFX::DoubleParam* m_RGB2XYZ_ZR;
    OFX::DoubleParam* m_RGB2XYZ_ZG;
    OFX::DoubleParam* m_RGB2XYZ_ZB;
    OFX::DoubleParam* m_LUMINANCE_BOOST;
    OFX::DoubleParam* m_ALPHA;
};

HDRPlugin::HDRPlugin(OfxImageEffectHandle p_Handle)
    : ImageEffect(p_Handle)
{
    m_DstClip = fetchClip(kOfxImageEffectOutputClipName);
    m_SrcClip = fetchClip(kOfxImageEffectSimpleSourceClipName);

    m_RGB2XYZ_XR = fetchDoubleParam("RGB2XYZ_XR");
    m_RGB2XYZ_XG = fetchDoubleParam("RGB2XYZ_XG");
    m_RGB2XYZ_XB = fetchDoubleParam("RGB2XYZ_XB");
    m_RGB2XYZ_YR = fetchDoubleParam("RGB2XYZ_YR");
    m_RGB2XYZ_YG = fetchDoubleParam("RGB2XYZ_YG");
    m_RGB2XYZ_YB = fetchDoubleParam("RGB2XYZ_YB");
    m_RGB2XYZ_ZR = fetchDoubleParam("RGB2XYZ_ZR");
    m_RGB2XYZ_ZG = fetchDoubleParam("RGB2XYZ_ZG");
    m_RGB2XYZ_ZB = fetchDoubleParam("RGB2XYZ_ZB");
    m_LUMINANCE_BOOST = fetchDoubleParam("LUMINANCE_BOOST");
    m_ALPHA = fetchDoubleParam("ALPHA");

    // Set the enabledness of our sliders
    setEnabledness();
}

void HDRPlugin::render(const OFX::RenderArguments& p_Args)
{
    if ((m_DstClip->getPixelDepth() == OFX::eBitDepthFloat) && (m_DstClip->getPixelComponents() == OFX::ePixelComponentRGBA))
    {
        ImageScaler imageScaler(*this);
        setupAndProcess(imageScaler, p_Args);
    }
    else
    {
        OFX::throwSuiteStatusException(kOfxStatErrUnsupported);
    }
}

bool HDRPlugin::isIdentity(const OFX::IsIdentityArguments& p_Args, OFX::Clip*& p_IdentityClip, double& p_IdentityTime)
{
//     double rScale = 1.0, gScale = 1.0, bScale = 1.0, aScale = 1.0;
//
//     if (m_ComponentScalesEnabled->getValueAtTime(p_Args.time))
//     {
//         rScale = m_ScaleR->getValueAtTime(p_Args.time);
//         gScale = m_ScaleG->getValueAtTime(p_Args.time);
//         bScale = m_ScaleB->getValueAtTime(p_Args.time);
//         aScale = m_ScaleA->getValueAtTime(p_Args.time);
//     }
//
//     const double scale = m_Scale->getValueAtTime(p_Args.time);
//     rScale *= scale;
//     gScale *= scale;
//     bScale *= scale;
//
//     if ((rScale == 1.0) && (gScale == 1.0) && (bScale == 1.0) && (aScale == 1.0))
//     {
//         p_IdentityClip = m_SrcClip;
//         p_IdentityTime = p_Args.time;
//         return true;
//     }

    return false;
}

void HDRPlugin::changedParam(const OFX::InstanceChangedArgs& p_Args, const std::string& p_ParamName)
{

}

void HDRPlugin::changedClip(const OFX::InstanceChangedArgs& p_Args, const std::string& p_ClipName)
{
    if (p_ClipName == kOfxImageEffectSimpleSourceClipName)
    {
        setEnabledness();
    }
}

void HDRPlugin::setEnabledness()
{
    // the component enabledness depends on the clip being RGBA
    const bool enable = (m_SrcClip->getPixelComponents() == OFX::ePixelComponentRGBA);

    m_RGB2XYZ_XR->setEnabled(enable);
    m_RGB2XYZ_XG->setEnabled(enable);
    m_RGB2XYZ_XB->setEnabled(enable);

    m_RGB2XYZ_YR->setEnabled(enable);
    m_RGB2XYZ_YG->setEnabled(enable);
    m_RGB2XYZ_YB->setEnabled(enable);

    m_RGB2XYZ_ZR->setEnabled(enable);
    m_RGB2XYZ_ZG->setEnabled(enable);
    m_RGB2XYZ_ZB->setEnabled(enable);

    m_LUMINANCE_BOOST->setEnabled(enable);
    m_ALPHA->setEnabled(enable);
}

void HDRPlugin::setupAndProcess(ImageScaler& p_ImageScaler, const OFX::RenderArguments& p_Args)
{
    // Get the dst image
    std::auto_ptr<OFX::Image> dst(m_DstClip->fetchImage(p_Args.time));
    OFX::BitDepthEnum dstBitDepth = dst->getPixelDepth();
    OFX::PixelComponentEnum dstComponents = dst->getPixelComponents();

    // Get the src image
    std::auto_ptr<OFX::Image> src(m_SrcClip->fetchImage(p_Args.time));
    OFX::BitDepthEnum srcBitDepth = src->getPixelDepth();
    OFX::PixelComponentEnum srcComponents = src->getPixelComponents();

    // Check to see if the bit depth and number of components are the same
    if ((srcBitDepth != dstBitDepth) || (srcComponents != dstComponents))
    {
        OFX::throwSuiteStatusException(kOfxStatErrValue);
    }

    double xr, xg, xb;
    double yr, yg, yb;
    double zr, zg, zb;
    double lum, alpha;

    xr = m_RGB2XYZ_XR->getValueAtTime(p_Args.time);
    xg = m_RGB2XYZ_XG->getValueAtTime(p_Args.time);
    xb = m_RGB2XYZ_XB->getValueAtTime(p_Args.time);

    yr = m_RGB2XYZ_YR->getValueAtTime(p_Args.time);
    yg = m_RGB2XYZ_YG->getValueAtTime(p_Args.time);
    yb = m_RGB2XYZ_YB->getValueAtTime(p_Args.time);

    zr = m_RGB2XYZ_ZR->getValueAtTime(p_Args.time);
    zg = m_RGB2XYZ_ZG->getValueAtTime(p_Args.time);
    zb = m_RGB2XYZ_ZB->getValueAtTime(p_Args.time);

    lum = m_LUMINANCE_BOOST->getValueAtTime(p_Args.time);
    alpha = m_ALPHA->getValueAtTime(p_Args.time);

    // Set the images
    p_ImageScaler.setDstImg(dst.get());
    p_ImageScaler.setSrcImg(src.get());

    // Setup OpenCL and CUDA Render arguments
    p_ImageScaler.setGPURenderArgs(p_Args);

    // Set the render window
    p_ImageScaler.setRenderWindow(p_Args.renderWindow);

    // Set the scales
    p_ImageScaler.setParams(xr, xg, xb, yr, yg, yb, zr, zg, zb, lum, alpha);

    // Call the base class process member, this will call the derived templated process code
    p_ImageScaler.process();
}

////////////////////////////////////////////////////////////////////////////////

using namespace OFX;

HDRPluginFactory::HDRPluginFactory()
    : OFX::PluginFactoryHelper<HDRPluginFactory>(kPluginIdentifier, kPluginVersionMajor, kPluginVersionMinor)
{
}

void HDRPluginFactory::describe(OFX::ImageEffectDescriptor& p_Desc)
{
    // Basic labels
    p_Desc.setLabels(kPluginName, kPluginName, kPluginName);
    p_Desc.setPluginGrouping(kPluginGrouping);
    p_Desc.setPluginDescription(kPluginDescription);

    // Add the supported contexts, only filter at the moment
    p_Desc.addSupportedContext(eContextFilter);
    p_Desc.addSupportedContext(eContextGeneral);

    // Add supported pixel depths
    p_Desc.addSupportedBitDepth(eBitDepthFloat);

    // Set a few flags
    p_Desc.setSingleInstance(false);
    p_Desc.setHostFrameThreading(false);
    p_Desc.setSupportsMultiResolution(kSupportsMultiResolution);
    p_Desc.setSupportsTiles(kSupportsTiles);
    p_Desc.setTemporalClipAccess(false);
    p_Desc.setRenderTwiceAlways(false);
    p_Desc.setSupportsMultipleClipPARs(kSupportsMultipleClipPARs);

    // Setup OpenCL and CUDA render capability flags
    p_Desc.setSupportsOpenCLRender(true);
    p_Desc.setSupportsCudaRender(false);
}

static DoubleParamDescriptor* defineScaleParam(OFX::ImageEffectDescriptor& p_Desc, const std::string& p_Name, const std::string& p_Label,
                                               const std::string& p_Hint, GroupParamDescriptor* p_Parent, float p_Default)
{
	DoubleParamDescriptor* param = p_Desc.defineDoubleParam(p_Name);
	param->setLabels(p_Label, p_Label, p_Label);
	param->setScriptName(p_Name);
	param->setHint(p_Hint);
	param->setDefault(p_Default);
	param->setRange(0, 1000);
	param->setIncrement(0.1);
	param->setDisplayRange(0, 2);
	param->setDoubleType(eDoubleTypeScale);

    if (p_Parent)
    {
        param->setParent(*p_Parent);
    }

    return param;
}


void HDRPluginFactory::describeInContext(OFX::ImageEffectDescriptor& p_Desc, OFX::ContextEnum /*p_Context*/)
{
    // Source clip only in the filter context
    // Create the mandated source clip
    ClipDescriptor* srcClip = p_Desc.defineClip(kOfxImageEffectSimpleSourceClipName);
    srcClip->addSupportedComponent(ePixelComponentRGBA);
    srcClip->setTemporalClipAccess(false);
    srcClip->setSupportsTiles(kSupportsTiles);
    srcClip->setIsMask(false);

    // Create the mandated output clip
    ClipDescriptor* dstClip = p_Desc.defineClip(kOfxImageEffectOutputClipName);
    dstClip->addSupportedComponent(ePixelComponentRGBA);
    dstClip->addSupportedComponent(ePixelComponentAlpha);
    dstClip->setSupportsTiles(kSupportsTiles);

    // Make some pages and to things in
    PageParamDescriptor* page = p_Desc.definePageParam("Controls");

    // Group param to group the transform parameters
    GroupParamDescriptor* componentScalesGroup = p_Desc.defineGroupParam("componentScales");
    componentScalesGroup->setHint("Scales on the individual transform components");
    componentScalesGroup->setLabels("Components", "Components", "Components");

    // Make luminance and alpha scale params
    DoubleParamDescriptor* param_lum = defineScaleParam(p_Desc, "LUMINANCE_BOOST", "Luminance Boost", "Scales the luminance in the image", 0, 1.0);
    page->addChild(*param_lum);
    DoubleParamDescriptor* param_alpha = defineScaleParam(p_Desc, "ALPHA", "Alpha", "Scales the alpha in the image", 0, 0.0376);
    page->addChild(*param_alpha);

    // Make the transform params
    DoubleParamDescriptor* param;
    param = defineScaleParam(p_Desc, "RGB2XYZ_XR", "XR", "Scales the XR component in the transform", componentScalesGroup, 0.412137);
    page->addChild(*param);
    param = defineScaleParam(p_Desc, "RGB2XYZ_XG", "XG", "Scales the XG component in the transform", componentScalesGroup, 0.376845);
    page->addChild(*param);
    param = defineScaleParam(p_Desc, "RGB2XYZ_XB", "XB", "Scales the XB component in the transform", componentScalesGroup, 0.142235);
    page->addChild(*param);

    param = defineScaleParam(p_Desc, "RGB2XYZ_YR", "YR", "Scales the YR component in the transform", componentScalesGroup, 0.221852);
    page->addChild(*param);
    param = defineScaleParam(p_Desc, "RGB2XYZ_YG", "YG", "Scales the YG component in the transform", componentScalesGroup, 0.757303);
    page->addChild(*param);
    param = defineScaleParam(p_Desc, "RGB2XYZ_YB", "YB", "Scales the YB component in the transform", componentScalesGroup, 0.081677);
    page->addChild(*param);

    param = defineScaleParam(p_Desc, "RGB2XYZ_ZR", "ZR", "Scales the ZR component in the transform", componentScalesGroup, 0.011588);
    page->addChild(*param);
    param = defineScaleParam(p_Desc, "RGB2XYZ_ZG", "ZG", "Scales the ZG component in the transform", componentScalesGroup, 0.069829);
    page->addChild(*param);
    param = defineScaleParam(p_Desc, "RGB2XYZ_ZB", "ZB", "Scales the ZB component in the transform", componentScalesGroup, 0.706036);
    page->addChild(*param);
}

ImageEffect* HDRPluginFactory::createInstance(OfxImageEffectHandle p_Handle, ContextEnum /*p_Context*/)
{
    return new HDRPlugin(p_Handle);
}

void OFX::Plugin::getPluginIDs(PluginFactoryArray& p_FactoryArray)
{
    static HDRPluginFactory hdrPlugin;
    p_FactoryArray.push_back(&hdrPlugin);
}
